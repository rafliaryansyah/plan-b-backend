<?php

use App\Loan;
use Illuminate\Support\Facades\Route;

Route::prefix('v1')->group(function()
{
    Route::prefix('auth')->group(function()
    {
        Route::post('backdoor/register', 'Auth\AuthController@superRegister');
        Route::post('login', 'Auth\AuthController@login'); 
    });
    Route::middleware(['auth:api'])->group(function()
    {
       Route::namespace('Registrations')->group(function()
       {
           // Member
            Route::get('anggota', 'MemberController@index');
            Route::post('anggota/create', 'MemberController@store');
            Route::patch('anggota/{id}/update', 'MemberController@update');
            Route::delete('anggota/{id}/delete', 'MemberController@destroy');

           // UserRole
            Route::get('user_role', 'UserRoleController@index');
            Route::post('user_role/create', 'UserRoleController@store');
            Route::patch('user_role/{id}/update', 'UserRoleController@update');
            Route::delete('user_role/{id}/delete', 'UserRoleController@destroy');
           
            // Nasabah Pinjam
            Route::get('simpanan', 'SavingCustomerController@index');
            Route::post('simpanan/create', 'SavingCustomerController@store');
            Route::patch('simpanan/{id}/update', 'SavingCustomerController@update')->name('savings.update');
            Route::delete('simpanan/{id}/delete', 'SavingCustomerController@destroy')->name('savings.delete');

            // Nasabah Pembiayaan
            Route::get('pembiayaan', 'FinancingCustomerController@index');
            Route::post('pembiayaan/create', 'FinancingCustomerController@store');
            Route::patch('pembiayaan/{id}/update', 'FinancingCustomerController@update')->name('financings.update');
            Route::delete('pembiayaan/{id}/delete', 'FinancingCustomerController@destroy')->name('financings.delete');
       });

       Route::namespace('Transactions')->prefix('transaksi')->group(function()
       {
          Route::post('simpanan', 'SavingsController'); 
          Route::post('penarikan', 'WithdrawalController');
          Route::post('pembiayaan', 'FinancingController');
          Route::post('pembayaran', 'PaymentController');
          Route::post('lainnya', 'OtherTransactionController');
       });
       Route::namespace('Reports')->prefix('laporan')->group(function()
       {
          Route::get('utama', 'ReportController@primaryReport');
          Route::get('lainnya', 'ReportController@otherReport');
       });
       Route::namespace('Information')->prefix('informasi')->group(function()
       {
          Route::get('nasabah_simpanan', 'CustomerBalanceController@customerSavings');
       });
    });
});