<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // Create role
        Role::create([
            'name' => 'Super Administrator',
            'guard_name' => 'api'
        ]);

        Role::create([
            'name' => 'Administrator',
            'guard_name' => 'api'
        ]);

        Role::create([
            'name' => 'Teller',
            'guard_name' => 'api'
        ]);

        Role::create([
            'name' => 'Collector',
            'guard_name' => 'api'
        ]);

        Role::create([
            'name' => 'User',
            'guard_name' => 'api'
        ]);
    }
}
