<?php

use App\Marriage;
use Illuminate\Database\Seeder;

class MarriageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Marriage::create([
            'name' => 'Belum Kawin',
            'slug' => \Str::slug('Belum Kawin')
        ]);

        Marriage::create([
            'name' => 'Kawin',
            'slug' => \Str::slug('Kawin')
        ]);
        
        Marriage::create([
            'name' => 'Cerai Hidup',
            'slug' => \Str::slug('Cerai Hidup')
        ]);

        Marriage::create([
            'name' => 'Cerai Mati',
            'slug' => \Str::slug('Cerai Mati')
        ]);


    }
}
