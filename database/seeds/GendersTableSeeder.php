<?php

use App\Gender;
use Illuminate\Database\Seeder;

class GendersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Gender::create([
            'name' => 'Laki-Laki',
            'slug' => \Str::slug('Laki-Laki'),
        ]);

        Gender::create([
            'name' => 'Perempuan',
            'slug' => \Str::slug('Perempuan'),
        ]);

        Gender::create([
            'name' => 'Lainnya',
            'slug' => \Str::slug('Lainnya'),
        ]);
    }
}
