<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'id_card_number' => '115234234123123',
            'first_name' => 'DummyName',
            'last_name' => 'DummyLast',
            'address' => 'Dummy Of Address',
            'birth_date' => now(),
            'birth_place' => 'Dummy',
            'phone' => '0888881231231',
            'marriage_id' => '2', // Relasi
            'religion_id' => '1', // Relasi
            'gender_id' => '1', // Relasi
            'email' => 'dummmyz@gmail.com',
            'password' => '$2b$10$PjHPznDLN7HvFS2saaFmeOQ/0hhdP89EHWkVf9SzdEHEN1z/R.Bsi', // 12345678,
        ]);
    }
}
