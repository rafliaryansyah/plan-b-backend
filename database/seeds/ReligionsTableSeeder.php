<?php

use App\Religion;
use Illuminate\Database\Seeder;

class ReligionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Religion::create([
            'name' => 'Islam',
            'slug' => \Str::slug('Islam')
        ]);

        Religion::create([
            'name' => 'Kristen Protestan',
            'slug' => \Str::slug('Kristen Protestan')
        ]);

        Religion::create([
            'name' => 'Kristen Katolik',
            'slug' => \Str::slug('Kristen Katolik')
        ]);

        Religion::create([
            'name' => 'Hindu',
            'slug' => \Str::slug('Hindu')
        ]);

        Religion::create([
            'name' => 'Buddha',
            'slug' => \Str::slug('Buddha')
        ]);
        
        Religion::create([
            'name' => 'Kong Hu Chu',
            'slug' => \Str::slug('Kong Hu Chu')
        ]);
    }
}
