<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('id_card_number');
            $table->string('address');
            $table->string('birth_date');
            $table->string('birth_place');
            $table->string('phone');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->integer('marriage_id')->foreign('marriage_id')->references('id')->on('marriages')->onDelete('cascade');
            $table->integer('religion_id')->foreign('religion_id')->references('id')->on('religions')->onDelete('cascade');
            $table->integer('gender_id')->foreign('gender_id')->references('id')->on('genders')->onDelete('cascade');
            $table->string('password')->nullable();
            $table->string('photo')->default('assets/images/default.jpg');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
