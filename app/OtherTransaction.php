<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtherTransaction extends Model
{
    protected $table = 'other_transactions';
    protected $fillable = ['product_id', 'amount', 'date', 'depositor', 'description', 'pic', 'transaction_type_id'];
    // protected $with = ['product'];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
