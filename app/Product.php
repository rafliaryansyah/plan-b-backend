<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['code', 'description', 'name', 'product_type_id'];
    public function otherTransactions()
    {
        return $this->hasMany(OtherTransaction::class, 'product_id');
    }

    public function productType()
    {
        return $this->belongsTo(ProductType::class, 'product_type_id');
    }
}
