<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';
    protected $date = ['date', 'created_at', 'updated_at'];
    protected $fillable = ['customer_id', 'amount', 'depositor', 'description', 'transaction_type_id', 'date'];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }
}
