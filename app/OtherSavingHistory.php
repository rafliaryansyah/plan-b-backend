<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtherSavingHistory extends Model
{
    protected $table = 'other_saving_histories';
    
}
