<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, HasRoles;

    protected $guard_name = 'api';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'id_card_number', 'address', 'birth_date', 'birth_place', 'phone', 'email', 'marriage_id', 'religion_id', 'gender_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
            'email' => $this->email
        ];
    }

    // Relasi to Marriage
    public function marriage()
    {
        return $this->belongsTo(Marriage::class, 'marriage_id');
    }

    // Relasi to Religion
    public function religion()
    {
        return $this->belongsTo(Religion::class, 'religion_id');
    }
    // Relasi to Gender
    public function gender()
    {
        return $this->belongsTo(Gender::class, 'gender_id');
    }

    // Relasi dengan customer
    public function customers()
    {
        return $this->hasMany(Customer::class, 'user_id');
    }
}
