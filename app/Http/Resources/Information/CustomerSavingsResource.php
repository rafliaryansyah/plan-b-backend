<?php

namespace App\Http\Resources\Information;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerSavingsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'account_number' => $this->customer->account_number,
            'name' => [
                'first' => $this->customer->user->first_name,
                'last' => $this->customer->user->last_name
            ],
            'product' => $this->customer_id,
            'saving_total' => $this->customer->savings_history,
            'saving_withdrawal' => $this->customer->savings_history,
            'balance' => $this->customer->savings_history,
        ];
    }
}
