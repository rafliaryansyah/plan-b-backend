<?php

namespace App\Http\Resources\Reports;

use Illuminate\Http\Resources\Json\JsonResource;

class PrimaryReportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => $this->created_at->format('l, d F Y'),
            'account_number' => $this->customer->account_number,
            'name' => [
                'first' => $this->customer->user->first_name,
                'last' => $this->customer->user->last_name
            ],
            'product' => $this->customer_id,
            'type' => $this->transaction_type_id,
            'total' => $this->amount,
        ];
    }
}
