<?php

namespace App\Http\Resources\Reports;

use Illuminate\Http\Resources\Json\JsonResource;

class OtherReportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'date' => $this->created_at->format('l, d F Y'),
            'transaction' => $this->product->name,
            'pic' => $this->pic,
            'type' => $this->product->productType->name,
            'description' => $this->description,
            'total' => $this->amount
        ];
    }
}
