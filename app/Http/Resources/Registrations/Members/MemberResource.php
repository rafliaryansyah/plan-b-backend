<?php

namespace App\Http\Resources\Registrations\Members;

use Illuminate\Http\Resources\Json\JsonResource;

class MemberResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'ktp' => $this->id_card_number,
            'name' => [
                'first' => $this->first_name,
                'last' => $this->last
            ],
            'gender' => $this->gender->name,
            'phone' => $this->phone,
            'address' => $this->address,
            'marriage_status' => $this->marriage->name,
            'religion' => $this->religion->name,
            'action' => [
                'update' => null,
                'delete' => null
            ]
        ];
    }
}
