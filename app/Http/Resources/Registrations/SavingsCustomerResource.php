<?php

namespace App\Http\Resources\Registrations;

use Illuminate\Http\Resources\Json\JsonResource;

class SavingsCustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'account_number' => $this->account_number,
            'ktp' => $this->user->id_card_number,
            'name' => [
                'first' => $this->user->first_name,
                'last' => $this->user->last_name,
            ],
            'product' => $this->product_id
        ];
    }
}
