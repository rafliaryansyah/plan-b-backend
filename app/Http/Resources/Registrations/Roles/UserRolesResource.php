<?php

namespace App\Http\Resources\Registrations\Roles;

use Illuminate\Http\Resources\Json\JsonResource;

class UserRolesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => [
                'first' => $this->first_name,
                'last' => $this->last_name
            ],
            'email' => $this->email,
            'role' => $this->roles->pluck('name')
        ];
    }
}
