<?php

namespace App\Http\Requests\UserRoles;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StoreUserRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => [
                'required', 'max:32'
            ],
            // 'id_card_number' => [
            //     'required',
            //     'max:16',
            //     Rule::unique('users', 'id_card_number')->ignore($this->id),
            // ],
            // 'email' => [
            //     'required',
            //     'max:72',
            //     Rule::unique('users', 'email')->ignore($this->id),
            //     'email'
            // ],
            'role' => [
                'required', 'in:Super Administrator,Administrator,Teller,Collector,User'
            ],
        ];
    }
}
