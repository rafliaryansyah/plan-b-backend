<?php

namespace App\Http\Requests\Members;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateMemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_card_number'    => [
                'required',
                Rule::unique('users', 'id_card_number')->ignore($this->id),
                'max:16',
            ],
            'first_name'        => [
                'required','max:32'
            ],
            'last_name'         => [
                'required', 'max:32'
            ],
            'birth_date'        => [
                'required', 'date'
            ],
            'birth_place'       => [
                'required', 'max:64'
            ],
            'phone'             => [
                'required',
                Rule::unique('users', 'phone')->ignore($this->id),
                'max:20'
            ],
            'email'             => [
                'nullable',
                'email',
                Rule::unique('users', 'email')->ignore($this->id),
                'max:64'
            ],
            'address'           => [
                'required', 'max:64'
            ],
            'gender_id'         => [
                'required'],
            'religion_id'       => ['required'
            ],
            'marriage_id'       => [
                'required'
            ]
        ];
    }
}
