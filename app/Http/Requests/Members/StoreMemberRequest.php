<?php

namespace App\Http\Requests\Members;

use Illuminate\Foundation\Http\FormRequest;

class StoreMemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_card_number'    => 'required|max:16|unique:users',
            'first_name'        => 'required|max:32',
            'last_name'         => 'required|max:32',
            'birth_date'        => 'required|date',
            'birth_place'       => 'required|max:64',
            'phone'             => 'required|unique:users|max:20',
            'email'             => 'nullable|email|unique:users|max:64',
            'address'           => 'required|max:64',
            'gender_id'         => 'required',
            'religion_id'       => 'required',
            'marriage_id'       => 'required'
        ];
    }
}
