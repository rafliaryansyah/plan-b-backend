<?php

namespace App\Http\Requests\SavingCustomers;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateSavingCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => [
                'required'
            ],
            'join_date' => [
                'required', 'date'
            ],
            'account_number' => [
                'required',
                Rule::unique('customers', 'account_number')->ignore($this->id),
            ],
            'product_id' => [
                'required'
            ],
            'office_id' => [
                'required'
            ]
        ];
    }
}
