<?php

namespace App\Http\Requests\FinancingCustomers;

use Illuminate\Foundation\Http\FormRequest;

class StoreFinancingCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'join_date' => 'required|date',
            'account_number' => 'required|max:32|unique:customers',
            'product_id' => 'required',
            'office_id' => 'required'
        ];
    }
}
