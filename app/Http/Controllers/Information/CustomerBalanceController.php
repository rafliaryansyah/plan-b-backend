<?php

namespace App\Http\Controllers\Information;

use App\Http\Controllers\Controller;
use App\Http\Resources\Information\CustomerSavingsResource;
use App\Transaction;
use Illuminate\Http\Request;

class CustomerBalanceController extends Controller
{
    // Saldo Nasabah Simpanan
    public function customerSavings()
    {
        // $balance_customer = Transaction::latest()->paginate(8);
        $balance_customer = Transaction::latest()->paginate(8);
        return CustomerSavingsResource::collection($balance_customer);
        
    }
}
