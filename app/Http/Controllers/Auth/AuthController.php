<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Register\CreateRegisterRequest;
use App\User;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'superRegister']]);
    }

    public function superRegister(CreateRegisterRequest $request)
    {
        // Jika user dengan role dengam Super Admin sudah ada, maka ditolak
        if (User::whereHas('roles', function ($q) {
            $q->where('name', 'Super Administrator');
        })->exists()) {
            return response()->json([
                'status'  => 403,
                'message' => 'Akses ditolak, akun sudah ada.',
            ], 403);
        }

        $register = User::create([
            'address' => 'Unknown', // Generate otomatis
            'birth_date' => '1971-01-01 00:00', // Generate otomatis
            'birth_place' => 'Unknown',  // Generate otomatis
            'phone' => '0888055550066', // Generate otomatis
            'marriage_id' => '0', // Generate otomatis
            'religion_id' => '0', // Generate otomatis
            'gender_id' => '0', // Generate otomatis
            'id_card_number' => '320000111222222', // Generate Otomatis
            'first_name' => request('first_name'),
            'last_name' => request('last_name'),
            'email' => request('email'),
            'password' => bcrypt(request('password'))
        ]);
        $register->assignRole('Super Administrator');

        return response()->json([
            'status' => 201,
            'message' => 'Akun berhasil dibuat, silahkan login'
        ], 201);
    }


    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json([
                'status' => 400,
                'message' => 'Username atau kata sandi salah dalam validasi.'
            ], 400);
        }

        return response()->json([
            'status' => 201,
            'message' => 'Anda berhasil masuk ke aplikasi.',
            'token' => $token
        ], 201);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
