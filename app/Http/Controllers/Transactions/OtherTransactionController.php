<?php

namespace App\Http\Controllers\Transactions;

use App\Http\Controllers\Controller;
use App\Http\Requests\Transactions\StoreOtherTransactionRequest;
use App\OtherSaving;
use App\OtherSavingHistory;
use App\OtherTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OtherTransactionController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(StoreOtherTransactionRequest $request)
    {
        // $transaction = OtherTransaction::first();
        // return $transaction->product->productType->id;
        DB::transaction(function () use ($request)
        {
            // Insert
            $other_transaction = OtherTransaction::create([
                'product_id' => $request->product_id,
                'depositor' => $request->depositor,
                'description' => $request->description,
                'date' => $request->date,
                'amount' => $request->amount,
                'transaction_type_id' => '5',
                'pic' => auth()->user()->email,
            ]);

            // Condition product
             if ($other_transaction->product->product_type_id == '1') {
                 // return
                // Create or Update Other Savings
                $check_balance = OtherSaving::first();
                if ($check_balance) {
                    $balance = OtherSaving::first();
                    $balance->balance = ($balance->balance + $request->amount);
                    $balance->save();
                } else {
                    $balance = new OtherSaving;
                    $balance->balance = $request->amount;
                    $balance->save();
                }
             } else if ($other_transaction->product->product_type_id == '2') {
                 // return 
                $check_balance = OtherSaving::first();
                if ($check_balance) {
                    $balance = OtherSaving::first();
                    $balance->balance = ($balance->balance - $request->amount);
                    $balance->save();
                } else {
                    $balance = new OtherSaving;
                    $balance->balance = $request->amount;
                    $balance->save();
                }
             } else {
                $check_balance = OtherSaving::first();
                if ($check_balance) {
                    $balance = OtherSaving::first();
                    $balance->balance = ($balance->balance + $request->amount);
                    $balance->save();
                } else {
                    $balance = new OtherSaving;
                    $balance->balance = $request->amount;
                    $balance->save();
                }
             }

             // Reduce savings Balance if type 2
            // if ($other_transaction->product->product_type_id == '2') {
            //     $saving = OtherSaving::first();
            //     $saving->balance = ($saving->balance - $request->amount);
            //     $saving->save();
            // }

            /// Insert into History OTher Savings
            $last_balance = OtherSaving::first();

            $saving_hitory = new OtherSavingHistory;
            $saving_hitory->date = now();
            if ($other_transaction->product->product_type_id == '1') {
                $saving_hitory->description = 'Pemasukan';
                $saving_hitory->income = $request->amount;
                $saving_hitory->balance = $last_balance->balance;
                $saving_hitory->save();
            } else if ($other_transaction->product->product_type_id == '2') {
                $saving_hitory->description = 'Pengeluaran';
                $saving_hitory->spending = $request->amount;
                $saving_hitory->balance = $last_balance->balance;
                $saving_hitory->save();
            }
        });

        return response()->json([
            'status' => 202,
            'message' => 'Data Transaksi Lainnya berhasil dilakukan'
        ], 202);
    }
}
