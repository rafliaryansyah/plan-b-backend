<?php

namespace App\Http\Controllers\Transactions;

use App\Http\Controllers\Controller;
use App\Http\Requests\Transactions\Savings\StoreRequest;
use App\{Saving, SavingHistory, Transaction};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SavingsController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(StoreRequest $request)
    {
        DB::transaction(function() use ($request)
        {
            // Insert into Transaction Savings
            $deposit = new Transaction;
            $deposit->customer_id = $request->customer;
            $deposit->date = $request->date;
            $deposit->depositor = $request->depositor;
            $deposit->description = $request->description;
            $deposit->amount = $request->amount;
            $deposit->transaction_type_id = '1';
            $deposit->save();

            // Create or Update Savings
            $check_balance = Saving::where('customer_id', $request->customer)->first();
            if ($check_balance) {
                $balance = Saving::where('customer_id', $request->customer)->first();
                $balance->balance = ($balance->balance + $request->amount);
                $balance->save();
            } else {
                $balance = new Saving;
                $balance->customer_id = $request->customer;
                $balance->balance = $request->amount;
                $balance->save();
            }

            // Insert into History Savings
            $last_balance = Saving::where('customer_id', $request->customer)->first();

            $saving_hitory = new SavingHistory;
            $saving_hitory->customer_id = $request->customer;
            $saving_hitory->date = now();
            $saving_hitory->description = 'Simpanan';
            $saving_hitory->debit = $request->amount;
            $saving_hitory->balance = $last_balance->balance;
            $saving_hitory->save();

        });

        return response()->json([
            'status' => 201,
            'message' => 'Data Transaksi Simpanan berhasil dilakukan.'
        ], 201);
    }
}
