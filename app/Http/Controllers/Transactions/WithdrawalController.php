<?php

namespace App\Http\Controllers\Transactions;

use App\Http\Controllers\Controller;
use App\Http\Requests\Transactions\StoreWithdrawalRequest;
use App\Saving;
use App\SavingHistory;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WithdrawalController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(StoreWithdrawalRequest $request)
    {
        // Balance check
        // if the withdrawal amount is greater than the balance
        // cancel the withdrawal process
        $balance_check = Saving::where('customer_id', $request->customer)->first();
        if ($balance_check) {
            if ($request->amount > $balance_check->balance) {
                return response()->json([
                    'status' => 400,
                    'message' => 'Saldo simpanan tidak mencukupi!'
                ],400);
            } 
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Saldo simpanan tidak mencukupi!'
            ],400);
        }

        DB::transaction(function() use ($request)
        {
            // Insert into Transaction Savings
            $withdrawal = new Transaction;
            $withdrawal->customer_id = $request->customer;
            $withdrawal->date = $request->date;
            $withdrawal->depositor = $request->depositor;
            $withdrawal->description = $request->description;
            $withdrawal->amount = $request->amount;
            $withdrawal->transaction_type_id = '1';
            $withdrawal->save();

            // Reduce savings Balance
            $saving = Saving::where('customer_id', $request->customer)->first();
            $saving->balance = ($saving->balance - $request->amount);
            $saving->save();

            // Insert into history saving
            $latest_saving = Saving::where('customer_id', $request->customer)->first();

            $saving_history = new SavingHistory;
            $saving_history->customer_id = $request->customer;
            $saving_history->date = now();
            $saving_history->description = 'Penarikan';
            $saving_history->debet = $request->amount;
            $saving_history->balance = $latest_saving->balance;
            $saving_history->save();

        });

        return response()->json([
            'status' => 202,
            'message' => 'Data Transaksi Penarikan berhasil dilakukan'
        ], 202);
    
    }
}
