<?php

namespace App\Http\Controllers\Transactions;

use App\Http\Controllers\Controller;
use App\Http\Requests\Transactions\StorePaymentRequest;
use App\Loan;
use App\LoanHistory;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(StorePaymentRequest $request)
    {
        $balance_check = Loan::where('customer_id', $request->customer)->first();
        if ($balance_check->remaining_loan == 0) {
            return response()->json([
                'status' => 400,
                'message' => 'Data Transaksi Pembiayaan Sudah lunas.'
            ], 400);
        }

        DB::transaction(function() use ($request)
        {
            // Insert into Transaction Savings
            $payment = new Transaction;
            $payment->customer_id = $request->customer;
            $payment->date = $request->date;
            $payment->depositor = $request->depositor;
            $payment->description = $request->description;
            $payment->amount = $request->amount;
            $payment->transaction_type_id = '4';
            $payment->save();

            // Reduce savings Balance
            $saving = Loan::where('customer_id', $request->customer)->first();
            $saving->remaining_loan = ($saving->remaining_loan - $request->amount);
            $saving->save();

            // Insert into history saving
            $latest_saving = Loan::where('customer_id', $request->customer)->first();

            $saving_history = new LoanHistory;
            $saving_history->customer_id = $request->customer;
            $saving_history->date = now();
            $saving_history->description = 'Pembayaran';
            $saving_history->debet = $request->amount;
            $saving_history->remainig_loan = $latest_saving->remaining_loan;
            $saving_history->save();

        });

        return response()->json([
            'status' => 202,
            'message' => 'Data Transaksi Pembayaran berhasil dilakukan'
        ], 202);
    }
}
