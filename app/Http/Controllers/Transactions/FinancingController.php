<?php

namespace App\Http\Controllers\Transactions;

use App\Http\Controllers\Controller;
use App\Http\Requests\Transactions\StoreFinancingRequest;
use App\Loan;
use App\LoanHistory;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FinancingController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(StoreFinancingRequest $request)
    {
        // Status check
        // if the payment still there
        // cancel the financing process
        $status_check = Loan::where('customer_id', $request->customer)->first();
        if ($status_check) {
            return response()->json([
                'status' => 400,
                'message' => 'Transaksi tidak dapat dilakukan, Pembiayaan sudah ada.'
            ], 400);
        }

        DB::transaction(function() use ($request)
        {
            // Insert into Transaction Financing type
            Transaction::create([
                'customer_id' => $request->customer,
                'date' => $request->date,
                'depositor' => $request->depositor,
                'description' => $request->description,
                'amount' => $request->amount,
                'transaction_type_id' => '3',
            ]);
            

            // Create Financing
            Loan::create([
                'customer_id' => $request->customer,
                'remaining_loan' => $request->amount
            ]);

            // Insert into History Loan
            $loans = Loan::where('customer_id', $request->customer)->first();

            $loan_history = new LoanHistory;
            $loan_history->customer_id = $request->customer;
            $loan_history->date = now();
            $loan_history->description = 'Pinjaman';
            $loan_history->debit = $request->amount;
            $loan_history->remainig_loan = $loans->remaining_loan;
            $loan_history->save();

        });

        return response()->json([
            'status' => 201,
            'message' => 'Data Transaksi Simpanan berhasil dilakukan.'
        ], 201);

    }
}
