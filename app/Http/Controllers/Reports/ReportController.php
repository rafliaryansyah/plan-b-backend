<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use App\Http\Resources\Reports\OtherReportResource;
use App\Http\Resources\Reports\PrimaryReportResource;
use App\OtherTransaction;
use App\Transaction;

class ReportController extends Controller
{
    public function primaryReport()
    {
        $primary_report = Transaction::where('transaction_type_id', '<=', '2')->latest()->get();
        return PrimaryReportResource::collection($primary_report);
    }

    public function otherReport()
    {
        $other_report = OtherTransaction::latest()->paginate();
        return OtherReportResource::collection($other_report);
    }
}
