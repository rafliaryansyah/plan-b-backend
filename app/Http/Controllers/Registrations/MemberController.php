<?php

namespace App\Http\Controllers\Registrations;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Members\StoreMemberRequest;
use App\Http\Requests\Members\UpdateMemberRequest;
use App\Http\Resources\Registrations\Members\MemberResource;
use Illuminate\Support\Facades\DB;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = User::paginate(5);
        return MemberResource::collection($members);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMemberRequest $request)
    {
        DB::table('users')->insert([
            'id_card_number' => $request->id_card_number,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'birth_date' => $request->birth_date,
            'birth_place' => $request->birth_place,
            'phone' => $request->phone,
            'email' => $request->email,
            'address' => $request->address,
            'gender_id' => $request->gender_id,
            'religion_id' => $request->religion_id,
            'marriage_id' => $request->marriage_id
        ]);

        return response()->json([
            'status' => 201,
            'message' => 'Data Member baru berhasil dibuat.'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMemberRequest $request, $id)
    {
        DB::table('users')->where('id', $id)->update([
            'id_card_number' => $request->id_card_number,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'birth_date' => $request->birth_date,
            'birth_place' => $request->birth_place,
            'phone' => $request->phone,
            'email' => $request->email,
            'address' => $request->address,
            'gender_id' => $request->gender_id,
            'religion_id' => $request->religion_id,
            'marriage_id' => $request->marriage_id
        ]);

        return response()->json([
            'status' => 200,
            'message' => 'Data Member berhasil diubah.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('users')->where('id', $id)->delete();

        return response()->json([
            'status' => 202,
            'message' => 'Data member berhasil dihapus.'
        ], 202);
    }
}
