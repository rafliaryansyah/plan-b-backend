<?php

namespace App\Http\Controllers\Registrations;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRoles\StoreUserRoleRequest;
use App\Http\Requests\UserRoles\UpdateUserRoleRequest;
use App\Http\Resources\Registrations\Roles\UserRolesResource;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class UserRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::whereHas('roles')->paginate(8);
        if ($users->count() > 0) {
            return UserRolesResource::collection($users);
        } else {
            return response()->json([
                'status' => 404,
                'message' => 'Data User Role tidak ditemukan'
            ], 404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRoleRequest $request)
    {
        $userRole = User::doesntHave('roles')
            ->where('first_name', request('first_name'))
            ->where('last_name', request('last_name'))
            ->firstOrFail();

        $userRole->assignRole([$request->role]);

        return response()->json([
            'status' => 201,
            'message' => 'Data User Role berhasil dibuat.'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRoleRequest $request, $id)
    {
        $userRole = User::whereHas('roles')->where('id', $id)
            ->where('first_name', request('first_name'))
            ->where('last_name', request('last_name'))
            ->firstOrFail();

        $userRole->syncRoles([$request->role]);

        return response()->json([
            'status' => 'success',
            'message' => 'Data User Role berhasil diubah.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::whereHas('roles')->where('id', $id)->delete();
        DB::table('model_has_roles')->where('model_id', $id)->delete();

        return response()->json([
            'status' => 202,
            'message' => 'Data User Role berhasil didelete.'
        ], 202);
    }
}
