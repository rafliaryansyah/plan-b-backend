<?php

namespace App\Http\Controllers\Registrations;

use App\Customer;
use App\Http\Controllers\Controller;
use App\Http\Requests\SavingCustomers\StoreSavingCustomerRequest;
use App\Http\Requests\SavingCustomers\UpdateSavingCustomerRequest;
use App\Http\Resources\Registrations\SavingsCustomerResource;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Claims\Custom;

class SavingCustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $savingsCustomer = Customer::where('customer_type_id', 1)->latest()->paginate(5);
        return SavingsCustomerResource::collection($savingsCustomer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSavingCustomerRequest $request)
    {
        Customer::create([
            'user_id' => $request->user_id,
            'join_date' => $request->join_date,
            'account_number' => $request->account_number,
            'product_id' => $request->product_id,
            'office_id' => $request->office_id,
            'customer_type_id' => '1'
        ]);

        return response()->json([
            'status' => 201,
            'message' => 'Data Nasabah Simpanan baru berhasil dibuat.'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSavingCustomerRequest $request, $id)
    {
        $updateSaving = Customer::where('id', $id)
            ->where('customer_type_id', 1)
            ->update([
                'user_id' => $request->user_id,
                'join_date' => $request->join_date,
                'account_number' => $request->account_number,
                'product_id' => $request->product_id,
                'office_id' => $request->office_id,
            ]);

        return response()->json([
            'status' => 200,
            'message' => 'Data Nasabah Simpanan Berhasil diubah.'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Customer::where('id', $id)
            ->where('customer_type_id', 1)
            ->delete();

        return response()->json([
            'status' => 202,
            'message' => 'Data Nasabah Simpanan berhasil dihapus.'
        ], 202);
    }
}
