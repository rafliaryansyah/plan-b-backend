<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtherSaving extends Model
{
    protected $table = 'other_savings';
}
