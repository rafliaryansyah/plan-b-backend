<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'account_number', 'customer_type_id', 'product_id', 'office_id', 'user_id', 'join_date'
    ];

    // Relasi ke user
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Get the saving record associated with the customer.
     */
    public function balance()
    {
        return $this->hasOne(Saving::class, 'customer_id');
    }

    /**
     * Get the saving record associated with the customer.
     */
    public function savings_history()
    {
        return $this->hasMany(SavingHistory::class, 'customer_id');
    }

    /**
     * Get the financing record associated with the customer.
     */
    public function loan()
    {
        return $this->hasOne(Loan::class, 'customer_id');
    }
    
    /**
     * Get the saving record associated with the customer.
     */
    public function loans_history()
    {
        return $this->hasMany(LoanHistory::class, 'customer_id');
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'customer_id');
    }
}
