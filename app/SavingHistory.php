<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SavingHistory extends Model
{
    protected $table = 'savings_history';
    protected $fillable = [];

    /**
     * Get the member that owns the saving history.
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }
}
